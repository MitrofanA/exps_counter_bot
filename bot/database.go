package bot

import (
	"database/sql"
	"log"
	"time"

	"bitbucket.org/py6jlb/budget_bot/config"
	_ "github.com/lib/pq" //init db driver
)

var Connection DatabaseConnection

type DatabaseConnection struct {
	db                 *sql.DB
	sqlAddExps         *sql.Stmt
	sqlSetCategory     *sql.Stmt
	sqlGetCategoryById *sql.Stmt
	sqlAddCategory     *sql.Stmt
	sqlGetCategories   *sql.Stmt
	sqlGetExpsCategory *sql.Stmt
}

type Category struct {
	CatId        int64
	CatName      string
	CatIcon      string
	CatHumanName string
}

func (connection *DatabaseConnection) Init(botConfig config.Config) {
	dbConn, err := sql.Open("postgres", botConfig.DbConnectionString)
	if err != nil {
		log.Fatal(err)
	}

	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
	}

	connection.db = dbConn
	connection.CreateTablesIfNotExists()
	connection.AlterTablesIfNeeded()
	connection.PrepareSqlStatements()
	connection.InitCategories(botConfig.Categories)
}

func (connection *DatabaseConnection) CreateTablesIfNotExists() {
	createSQL := `
		CREATE TABLE IF NOT EXISTS Expenses (
			exps_id SERIAL NOT NULL PRIMARY KEY,
			amount float NOT NULL,
			date_exps timestamp NOT NULL,
			user_name text NOT NULL
		);

		CREATE TABLE IF NOT EXISTS Categories (
			cat_id SERIAL NOT NULL PRIMARY KEY,
			cat_name text NOT NULL UNIQUE,
			cat_icon varchar(15) NOT NULL,
			cat_human_name text NOT NULL
		);
	`
	if rows, err := connection.db.Query(createSQL); err != nil {
		log.Fatal(err)
	} else {
		rows.Close()
	}
}

func (connection *DatabaseConnection) InitCategories(elems []config.CatInitial) {
	for _, elem := range elems {
		_, err := connection.sqlAddCategory.Exec(elem.Cat_name, elem.Icon, elem.Cat_human_name)
		if err != nil {
			log.Fatal(err)
			continue
		}
	}
}

func (connection *DatabaseConnection) AlterTablesIfNeeded() {
	alterSQL := "ALTER TABLE Expenses ADD COLUMN IF NOT EXISTS category_id int REFERENCES Categories (cat_id) NULL;"
	if rows, err := connection.db.Query(alterSQL); err != nil {
		log.Fatal(err)
	} else {
		rows.Close()
	}
}

func (connection *DatabaseConnection) PrepareSqlStatements() {
	var err error
	if connection.sqlAddExps, err = connection.db.Prepare(
		"INSERT INTO Expenses (amount, date_exps, user_name) VALUES ($1, $2, $3) " +
			"RETURNING exps_id",
	); err != nil {
		log.Fatal(err)
	}
	if connection.sqlSetCategory, err = connection.db.Prepare(
		"UPDATE Expenses SET category_id = $1 WHERE exps_id = $2"); err != nil {
		log.Fatal(err)
	}
	if connection.sqlGetCategoryById, err = connection.db.Prepare(
		"SELECT * FROM Categories WHERE cat_id = $1"); err != nil {
		log.Fatal(err)
	}
	if connection.sqlGetCategories, err = connection.db.Prepare(
		"SELECT * FROM Categories"); err != nil {
		log.Fatal(err)
	}
	if connection.sqlAddCategory, err = connection.db.Prepare(
		"INSERT INTO Categories (cat_name, cat_icon, cat_human_name) SELECT $1, $2, $3 " +
			"WHERE NOT EXISTS (SELECT cat_id FROM Categories WHERE cat_name = $1)"); err != nil {
		log.Fatal(err)
	}
	if connection.sqlGetExpsCategory, err = connection.db.Prepare(
		"SELECT category_id FROM Expenses WHERE exps_id = $1"); err != nil {
		log.Fatal(err)
	}
}

func (connection *DatabaseConnection) AddExps(amount float64, date time.Time, user string) (int64, error) {
	var id int64
	err := connection.sqlAddExps.QueryRow(amount, date, user).Scan(&id)
	return id, err
}

func (connection *DatabaseConnection) CheckCategoryExists(id int64) bool {
	var cat int64
	res := false
	connection.sqlGetExpsCategory.QueryRow(id).Scan(&cat)
	if cat > 0 {
		res = true
	}
	return res
}

func (connection *DatabaseConnection) GetCategoryById(id int64) (Category, error) {
	var cat Category
	err := connection.sqlGetCategoryById.QueryRow(id).Scan(&cat.CatId, &cat.CatName, &cat.CatIcon, &cat.CatHumanName)
	return cat, err
}

func (connection *DatabaseConnection) GetCategories() []*Category {
	rows, err := connection.sqlGetCategories.Query()
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	result := make([]*Category, 0)

	for rows.Next() {
		scanRes := new(Category)
		err := rows.Scan(&scanRes.CatId, &scanRes.CatName, &scanRes.CatIcon, &scanRes.CatHumanName)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, scanRes)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func (connection *DatabaseConnection) SetCategory(catId int64, expId int64) error {
	if _, err := connection.sqlSetCategory.Exec(catId, expId); err != nil {
		return err
	}
	return nil
}
