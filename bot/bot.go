package bot

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gopkg.in/telegram-bot-api.v4"

	"bitbucket.org/py6jlb/budget_bot/config"
)

type TelegramBot struct {
	API             *tgbotapi.BotAPI
	Updates         <-chan tgbotapi.Update
	AllowChatIds    []int64
	ExpsChan        chan *tgbotapi.Message
	SetCategoryChan chan *tgbotapi.Update
	CommandMassages chan *tgbotapi.Message
	WrongMassages   chan *tgbotapi.Message
}

func (telegramBot *TelegramBot) Init(botConfig config.Config) {
	telegramBot.AllowChatIds = botConfig.AllowedChatIds
	botAPI, err := tgbotapi.NewBotAPI(botConfig.TelegramBotToken)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Authorized on account %s", botAPI.Self.UserName)
	telegramBot.API = botAPI
	botUpdate := tgbotapi.NewUpdate(botConfig.BotOffset) // Инициализация канала обновлений
	botUpdate.Timeout = botConfig.BotTimeOut
	botUpdates, err := telegramBot.API.GetUpdatesChan(botUpdate)
	if err != nil {
		log.Fatal(err)
	}
	telegramBot.Updates = botUpdates
	telegramBot.CommandMassages = make(chan *tgbotapi.Message)
	telegramBot.ExpsChan = make(chan *tgbotapi.Message)
	telegramBot.SetCategoryChan = make(chan *tgbotapi.Update)
	telegramBot.WrongMassages = make(chan *tgbotapi.Message)
}

func (telegramBot *TelegramBot) Start() {
	go telegramBot.HandleMessage()
	telegramBot.ListenUpdates()

}

func (telegramBot *TelegramBot) ListenUpdates() {
	for update := range telegramBot.Updates {
		if update.Message != nil {
			// Если сообщение есть и его длина больше 0 -> начинаем обработку
			userName := update.Message.From.UserName
			chatID := update.Message.Chat.ID
			text := update.Message.Text

			// Проверяем является ли этот чат разрешенным
			if !telegramBot.Auth(chatID) {
				reply := "Вам нельзя это делать."
				log.Println(reply)
				bot_msg := tgbotapi.NewMessage(chatID, reply)
				telegramBot.API.Send(bot_msg)
				continue
			}
			log.Printf("[%s] %d %s", userName, chatID, text)
			//Сортируем сообщения по каналам
			if update.Message.IsCommand() {
				telegramBot.CommandMassages <- update.Message
				continue
			}
			if telegramBot.IsFloat(text) {
				telegramBot.ExpsChan <- update.Message
				continue
			}
			if !telegramBot.IsFloat(text) && !update.Message.IsCommand() {
				telegramBot.WrongMassages <- update.Message
				continue
			}
		}
		//Если фоновый запрос то в отдельный канал
		if update.CallbackQuery != nil {
			telegramBot.SetCategoryChan <- &update
			continue
		}
	}
}

func (telegramBot *TelegramBot) Auth(chatId int64) bool {
	for _, allowChatId := range telegramBot.AllowChatIds {
		if chatId == allowChatId {
			return true
		}
	}
	return false
}

//Проверяем являетються ли введенные данные расходами 
func (telegramBot *TelegramBot) IsFloat(text string) bool {
	res, err := strconv.ParseFloat(text, 64)
	if err == nil && res > 0 {
		return true
	}
	return false
}

//А здесь мы запусаем обработку сообщений
func (telegramBot *TelegramBot) HandleMessage() {
	for {
		select {
		case msg := <-telegramBot.CommandMassages:
			telegramBot.HandleCommand(msg)
		case msg := <-telegramBot.ExpsChan:
			telegramBot.HandleAmount(msg)
		case msg := <-telegramBot.SetCategoryChan:
			telegramBot.HandleCalback(msg)
		case msg := <-telegramBot.WrongMassages:
			telegramBot.HandleWrong(msg)
		}
	}
}

func (telegramBot *TelegramBot) HandleCommand(msg *tgbotapi.Message) {
	botMsg := tgbotapi.NewMessage(msg.Chat.ID, "")
	switch msg.Command() {
	case "start", "help":
		botMsg.Text = "Введи сумму и выбери категорию расхода."
	case "close":
		botMsg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	default:
		botMsg.Text = "Чето не то ты вводишь, я такого неумею."
	}
	telegramBot.API.Send(botMsg)
}

func (telegramBot *TelegramBot) HandleAmount(msg *tgbotapi.Message) {
	exps, _ := strconv.ParseFloat(msg.Text, 64)
	botMsg := tgbotapi.NewMessage(msg.Chat.ID, "")
	//if id, err := Connection.AddExps(exps, time.Now(), msg.Chat.UserName); err == nil {
	if id, err := Connection.AddExps(exps, msg.Time(), msg.Chat.UserName); err == nil {
		botMsg.Text = msg.Text
		botMsg.ReplyMarkup = telegramBot.NewCatKeyboard(id)
	} else {
		log.Fatal("Error on insert exps", err)
	}
	telegramBot.API.Send(botMsg)
}

//Формируем новую инлайн клавиатуру
func (telegramBot *TelegramBot) NewCatKeyboard(exps_id int64) tgbotapi.InlineKeyboardMarkup {
	categorySet := Connection.GetCategories()
	rows := make([][]tgbotapi.InlineKeyboardButton, 0)
	row := make([]tgbotapi.InlineKeyboardButton, 0)
	counter := 1
	for _, category := range categorySet {
		if (counter % 7) != 0 {
			row = append(row, tgbotapi.NewInlineKeyboardButtonData(category.CatIcon,
				fmt.Sprintf(`{"exps_id":%[1]d,"category_id":%[2]d}`, exps_id, category.CatId)))
			counter++
		}
		if (counter % 7) == 0 {
			rows = append(rows, row)
			row = make([]tgbotapi.InlineKeyboardButton, 0)
			row = append(row, tgbotapi.NewInlineKeyboardButtonData(category.CatIcon,
				fmt.Sprintf(`{"exps_id":%[1]d,"category_id":%[2]d}`, exps_id, category.CatId)))
			counter++
		}
	}
	keyboard := tgbotapi.NewInlineKeyboardMarkup()
	keyboard.InlineKeyboard = rows
	return keyboard
}

func (telegramBot *TelegramBot) HandleWrong(msg *tgbotapi.Message) {
	botMsg := tgbotapi.NewMessage(msg.Chat.ID, "Нужно вводить только положительные цифры или команды. Примеры: 175.50, /help")
	telegramBot.API.Send(botMsg)
}

//Тип для парсинга данных из фонового запроса
type callbackData struct {
	ExpsID int64 `json:"exps_id"`
	CatID  int64 `json:"category_id"`
}

func (telegramBot *TelegramBot) HandleCalback(upd *tgbotapi.Update) {
	callbackConfig := tgbotapi.CallbackConfig{
		CallbackQueryID: upd.CallbackQuery.ID,
		ShowAlert:       false,
	}

	var data callbackData
	jsonData := upd.CallbackQuery.Data
	err := json.Unmarshal([]byte(jsonData), &data)
	if err != nil {
		log.Fatal(err)
	}

	setedCat, err := Connection.GetCategoryById(data.CatID)
	if err != nil {
		log.Fatal("+", err)
	}

	if Connection.CheckCategoryExists(data.ExpsID) {
		callbackConfig.Text = "Этим расходам уже была установлена категория"
	} else {
		if err := Connection.SetCategory(data.CatID, data.ExpsID); err != nil {
			log.Fatal("-", err)
		}

		callbackConfig.Text = fmt.Sprintf("Категория %s, была установлена", setedCat.CatHumanName)
		messageTextConfig := tgbotapi.NewEditMessageText(upd.CallbackQuery.Message.Chat.ID, upd.CallbackQuery.Message.MessageID, fmt.Sprintf("%s: %s", upd.CallbackQuery.Message.Text, setedCat.CatIcon))
		_, err = telegramBot.API.Send(messageTextConfig)
		if err != nil {
			log.Fatal("222", err)
		}
	}
	//Отвечаем на фоновй запрос, если ид протух то просто пишем в лог и ничего не говорим
	if _, err := telegramBot.API.AnswerCallbackQuery(callbackConfig); err != nil {
		if strings.Contains(err.Error(), "QUERY_ID_INVALID") {
			log.Println("---", err)
			return
		} else {
			log.Fatal("---", err)
		}
	}
}
