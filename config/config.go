package config

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	DebugMode          bool         `json:"bot_debug"`
	TelegramBotToken   string       `json:"telegram_bot_token"`
	AllowedChatIds     []int64      `json:"allowed_chat_ids"`
	DbConnectionString string       `json:"db_connection_string"`
	BotOffset          int          `json:"bot_offset"`
	BotTimeOut         int          `json:"bot_timeout"`
	Categories         []CatInitial `json:"keyboard"`
}

type CatInitial struct {
	Icon           string `json:"icon"`
	Cat_name       string `json:"cat_name"`
	Cat_human_name string `json:"cat_human_name"`
}

func (c *Config) New(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	if err != nil {
		log.Fatal(err)
	}
}
