package main

import (
	"bitbucket.org/py6jlb/budget_bot/bot"
	"bitbucket.org/py6jlb/budget_bot/config"
)

var telegramBot bot.TelegramBot
var botConfig config.Config

func main() {
	//создаем конфиг
	botConfig.New("config.json")
	//инициализируем базу
	bot.Connection.Init(botConfig)
	//инициализируем бота
	telegramBot.Init(botConfig)
	//стартуем бота
	telegramBot.Start()
}
